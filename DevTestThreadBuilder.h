///-----------------------------------------------------------------
///
/// @file      DevTestThreadBuilder.h
/// @author    Ken
/// Created:   5/03/2013 9:48:37 AM
/// @section   DESCRIPTION
///            DevTestThreadBuilder class declaration
///
///------------------------------------------------------------------

#ifndef __DEVTESTTHREADBUILDER_H__
#define __DEVTESTTHREADBUILDER_H__

#ifdef __BORLANDC__
	#pragma hdrstop
#endif

#ifndef WX_PRECOMP
	#include <wx/wx.h>
	#include <wx/frame.h>
#else
	#include <wx/wxprec.h>
#endif

//Do not add custom headers between
//Header Include Start and Header Include End.
//wxDev-C++ designer will remove them. Add custom headers after the block.
////Header Include Start
#include <wx/button.h>
#include <wx/choice.h>
#include <wx/spinctrl.h>
#include <wx/textctrl.h>
#include <wx/stattext.h>
#include <wx/sizer.h>
#include <wx/panel.h>
#include <wx/html/htmlwin.h>
#include <wx/checkbox.h>

////Header Include End

////Dialog Style Start
#undef DevTestThreadBuilder_STYLE
#define DevTestThreadBuilder_STYLE wxCAPTION | wxSYSTEM_MENU | wxMINIMIZE_BOX | wxMAXIMIZE_BOX | wxCLOSE_BOX | wxRESIZE_BORDER
////Dialog Style End

class DevTestThreadBuilder : public wxFrame
{
	private:
		DECLARE_EVENT_TABLE();

	public:
		void WxChoiceInstallTypeChanged(wxCommandEvent& event);
		void WxChoicePermissionTypeChanged(wxCommandEvent& event);
		void WxChoice1Selected(wxCommandEvent& event);
		DevTestThreadBuilder(wxWindow *parent, wxWindowID id = 1, const wxString &title = wxT("Development Test Thread Builder"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize, long style = DevTestThreadBuilder_STYLE);
		virtual ~DevTestThreadBuilder();
		void OnCtrlUpdate(wxCommandEvent& event);
		void OnSpinCtrlUpdate(wxSpinEvent& event);
		void Mnuwebsite1104Click(wxCommandEvent& event);
		void Mnuupdate1106Click(wxCommandEvent& event);
		void Mnuexit1102Click(wxCommandEvent& event);
		void Mnuabout1105Click(wxCommandEvent& event);
		void CheckForUpdates(bool bCheckSilently);
		void OnCheckUpdateThread(wxCommandEvent& event);
		void CopyToClipboard(wxString text);
		void OnCopyTitleClick(wxCommandEvent& event);
		void OnCopyBodyClick(wxCommandEvent& event);
		void UpdateOutput();

	private:
//		float fDownloadSize;
//		float fInstallSize;
//		float fAdditionalSize;
		//Do not add custom control declarations between
		//GUI Control Declaration Start and GUI Control Declaration End.
		//wxDev-C++ will remove them. Add custom code after the block.
		////GUI Control Declaration Start
		wxMenuBar *WxMenuBar1;
		wxCheckBox * WxCheckBoxReleaseNotes;
//		wxCheckBox * WxCheckBoxOnlineInstaller;
		wxChoice *WxChoiceInstallType;
		wxTextCtrl *WxEditDescription;
		wxTextCtrl *WxEditMD5;
		wxStaticText *WxStaticText18;
		wxStaticText *WxStaticText17;
		wxStaticText *WxStaticText16;
		wxStaticText *WxStaticText15;
		wxStaticText *WxStaticText14;
		wxStaticText *WxStaticText12;
		wxStaticText *WxStaticText11;
		wxStaticText *WxStaticText10;
		wxStaticText *WxStaticText9;
		wxStaticText *WxStaticText8;
		wxStaticText *WxStaticText7;
		wxStaticText *WxStaticText6;
		wxStaticText *WxStaticText5;
		wxStaticText *WxStaticText4;
		wxStaticText *WxStaticText3;
		wxStaticText *WxStaticText2;
		wxStaticText *WxStaticText1;
		wxTextCtrl *WxEditPermissionText;
		wxBoxSizer *WxBoxSizer26;
		wxPanel *WxPanel26;
		wxBoxSizer *WxBoxSizer25;
		wxPanel *WxPanel25;
		wxBoxSizer *WxBoxSizer24;
		wxPanel *WxPanel24;
		wxTextCtrl *WxEditPermissionLink;
		wxChoice *WxChoicePermissionType;
		wxBoxSizer *WxBoxSizer23;
		wxPanel *WxPanel23;
		wxTextCtrl *WxEditAdditionalSize;
		wxBoxSizer *WxBoxSizer22;
		wxPanel *WxPanel22;
		wxBoxSizer *WxBoxSizer21;
		wxPanel *WxPanel21;
		wxBoxSizer *WxBoxSizer20;
		wxPanel *WxPanel20;
		wxBoxSizer *WxBoxSizer19;
		wxPanel *WxPanel19;
		wxBoxSizer *WxBoxSizer18;
		wxPanel *WxPanel18;
		wxBoxSizer *WxBoxSizer17;
		wxPanel *WxPanel17;
		wxTextCtrl *WxEditInstallSize;
		wxBoxSizer *WxBoxSizer16;
		wxPanel *WxPanel16;
		wxTextCtrl *WxEditDownloadSize;
		wxBoxSizer *WxBoxSizer15;
		wxPanel *WxPanel15;
		wxTextCtrl *WxEditDownloadLink;
		wxBoxSizer *WxBoxSizer14;
		wxPanel *WxPanel14;
		wxBoxSizer *WxBoxSizer13;
		wxPanel *WxPanel13;
		wxBoxSizer *WxBoxSizer12;
		wxPanel *WxPanel12;
		wxTextCtrl *WxEditSourceCodeLink;
		wxBoxSizer *WxBoxSizer8;
		wxPanel *WxPanel8;
		wxChoice *WxChoiceLicence;
		wxBoxSizer *WxBoxSizer7;
		wxPanel *WxPanel7;
		wxBoxSizer *WxBoxSizer6;
		wxPanel *WxPanel6;
		wxChoice *WxChoiceCategory;
		wxBoxSizer *WxBoxSizer11;
		wxPanel *WxPanel11;
		wxSpinCtrl *WxSpinCtrl1;
		wxBoxSizer *WxBoxSizer5;
		wxPanel *WxPanel5;
		wxTextCtrl *WxEditAppVersion;
		wxBoxSizer *WxBoxSizer4;
		wxPanel *WxPanel4;
		wxBoxSizer *WxBoxSizer3;
		wxPanel *WxPanel3;
		wxTextCtrl *WxEditHomepage;
		wxBoxSizer *WxBoxSizer10;
		wxPanel *WxPanel10;
		wxTextCtrl *WxEditAppName;
		wxBoxSizer *WxBoxSizer9;
		wxPanel *WxPanel9;
		wxBoxSizer *WxBoxSizer2;
		wxPanel *WxPanel2;
		wxBoxSizer *WxBoxSizer1;
		wxPanel *WxPanel1;
		wxButton *WxButtonCopyBody;
		wxPanel *WxPanelR4;
		wxBoxSizer *WxBoxSizerR4;
		wxHtmlWindow *WxHtmlOutputBody;
		wxStaticText *WxStaticTextR2;
		wxPanel *WxPanelR2;
		wxBoxSizer *WxBoxSizerR2;
		wxHtmlWindow *WxHtmlOutputTitle;
		wxStaticText *WxStaticTextR1;
		wxPanel *WxPanelR3;
		wxBoxSizer *WxBoxSizerR3;
		wxButton *WxButtonCopyTitle;
		wxPanel *WxPanelR1;
		wxBoxSizer *WxBoxSizerR1;
		wxPanel *WxPanelRight;
		wxBoxSizer *WxBoxSizerRight;
		wxPanel *WxPanelCenter;
		wxBoxSizer *WxBoxSizerCenter;
		wxBoxSizer *WxBoxSizerBase;
		wxPanel *WxPanelBase;
		////GUI Control Declaration End

	private:
		//Note: if you receive any error with these enum IDs, then you need to
		//change your old form code that are based on the #define control IDs.
		//#defines may replace a numeric value for the enum names.
		//Try copy and pasting the below block in your old form header files.
		enum
		{
			////GUI Enum Control ID Start
			ID_MNU_FILE_1101 = 1101,
			ID_MNU_EXIT_1102 = 1102,
			ID_MNU_HELP_1103 = 1103,
			ID_MNU_WEBSITE_1104 = 1104,
			ID_MNU_ABOUT_1105 = 1105,
			ID_MNU_UPDATE_1106 = 1106,


			ID_WXPANELBASE = 1001,
			ID_WXPANEL1 = 1002,
			ID_WXPANEL2 = 1003,
			ID_WXPANEL3 = 1004,
			ID_WXPANEL4 = 1005,
			ID_WXPANEL5 = 1006,
			ID_WXPANEL6 = 1007,
			ID_WXPANEL7 = 1008,
			ID_WXPANEL8 = 1009,
			ID_WXPANEL9 = 1010,
			ID_WXPANEL10 = 1011,
			ID_WXPANEL11 = 1012,
			ID_WXPANEL12 = 1013,
			ID_WXPANEL13 = 1014,
			ID_WXPANEL14 = 1015,
			ID_WXPANEL15 = 1016,
			ID_WXPANEL16 = 1017,
			ID_WXPANEL17 = 1018,
			ID_WXPANEL18 = 1019,
			ID_WXPANEL19 = 1020,
			ID_WXPANELCENTER = 1021,
			ID_WXPANELRIGHT = 1022,
			ID_WXPANELR1 = 1023,
			ID_WXPANELR2 = 1024,
			ID_WXPANELR3 = 1025,
			ID_WXPANELR4 = 1026,
			ID_WXPANEL20 = 1027,
			ID_WXPANEL21 = 1028,
			ID_WXPANEL22 = 1029,
			ID_WXPANEL23 = 1030,
			ID_WXPANEL24 = 1070,
			ID_WXPANEL25 = 1073,
			ID_WXPANEL26 = 1075,

			ID_WXSTATICTEXT1 = 1031,
			ID_WXSTATICTEXT2 = 1032,
			ID_WXSTATICTEXT3 = 1033,
			ID_WXSTATICTEXT4 = 1034,
			ID_WXSTATICTEXT5 = 1035,
			ID_WXSTATICTEXT6 = 1036,
			ID_WXSTATICTEXT7 = 1037,
			ID_WXSTATICTEXT8 = 1038,
			ID_WXSTATICTEXT9 = 1039,
			ID_WXSTATICTEXT10 = 1040,
			ID_WXSTATICTEXT11 = 1041,
			ID_WXSTATICTEXT12 = 1042,
			ID_WXSTATICTEXT13 = 1043,
			ID_WXSTATICTEXTR1 = 1044,
			ID_WXSTATICTEXTR2 = 1045,
			ID_WXSTATICTEXT14 = 1046,
			ID_WXSTATICTEXT15 = 1047,
			ID_WXSTATICTEXT16 = 1048,
			ID_WXSTATICTEXT17 = 1071,
			ID_WXSTATICTEXT18 = 1074,
			ID_WXSTATICTEXT19 = 1076,


			ID_WXEDITAPPNAME = 1050,
			ID_WXEDITHOMEPAGE = 1051,
			ID_WXEDITAPPVERSION = 1052,
			ID_WXSPINCTRL1 = 1053,
			ID_WXCHOICECATEGORY = 1054,
			ID_WXCHOICELICENCE = 1055,
			ID_WXEDITSOURCECODELINK = 1056,
			ID_WXEDITDOWNLOADLINK = 1057,
			ID_WXEDITDOWNLOADSIZE = 1058,
			ID_WXEDITINSTALLSIZE = 1059,
			ID_WXEDITMD5 = 1060,
			ID_WXEDITDESCRIPTION = 1061,
			ID_WXCHECKBOXRELEASENOTES = 1062,
			ID_WXHTMLOUTPUTTITLE = 1063,
			ID_WXHTMLOUTPUTBODY = 1064,
			ID_WXBUTTONCOPYTITLE = 1065,
			ID_WXBUTTONCOPYBODY = 1066,
//			ID_WXCHECKBOXONLINEINSTALLER = 1067,
			ID_WXCHOICEINSTALLTYPE = 1067,
			ID_WXEDITADDITIONALSIZE = 1068,
			ID_WXEDITPERMISSIONLINK = 1069,
			ID_WXCHOICEPERMISSIONTYPE = 1072,
			ID_WXEDITPERMISSIONTEXT = 1077,

			////GUI Enum Control ID End
			ID_DUMMY_VALUE_ //don't remove this value unless you have other enum values
		};

	private:
		void OnClose(wxCloseEvent& event);
		void CreateGUIControls();
		bool bIsLookupRunning;
		bool bIsCheckingUpdates;

		wxString outputTitle;
		wxString outputBody;
};

#endif
